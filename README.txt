Thank you for downloading this theme!


ABOUT THE SALAMANDER THEME:
-------------------------------------------------------------------------+

The initial Salamander theme used concepts in Litejazz theme but no need for 
ThemeSettingsAPI Module to achieve the same functionality. The graphics 
were inspired by the Joomla's RoketTheme.

Since version 1.4 for Drupal 5, the Salamander theme has been redesigned to 
be fully compatible with Drupal 5 and Drupal 6. 

It is a very reliable theme with a fixed centred width or fluid width, 
supporting screen resolutions of 1024 pixels and up. The theme validates 
XHTML 1.0 Strict / CSS 2, and it is cross-browser compatible; works 
perfect in Firefox 2.0, IE 6, IE 7, Opera 9, Netscape 8.


MODULE SUPPORT
-------------------------------------------------------------------------+
This theme can support virtualy any module.
It has been heavily tested with:
  - AdSense module;
  - Image module;
  - Logintoboggan;
  - Panels;
  - TinyMCE;


THEME MODIFICATION
-------------------------------------------------------------------------+
If you feel like giving the theme a look of your own, I recommend:
  - replace the graphic files in the images/salamander1 folder, keeping dimensions.
  - and/or change the text colors in the stylesheet style.css.
  - you can also modify the page width (fixed or fluid) by changing the following 
  	line in style.css file: 
  		#page {width: 95%;} 	-> default fluid width... to
  		#page {width: 800px;} -> 800px fixed width

Salamander Skins theme will alow many sub-themes as plugins.


CONTACT
-------------------------------------------------------------------------+
My drupal nick is florian � and I can be reached at cfradut@gmail.com or 
florian@radut.net

I can also be contacted for paid customizations of Salamander theme as well as
Drupal consulting, installation and customizations.


The theme is installed in both versions for Drupal 5 and 6 at my sites: 
http://www.radut.net/
http://www.puzzle.ro/
http://www.smokersassociation.org/
http://www.boaz.ro/

View the entire sub-themes collection and use the theme switcher to view demos
at http://www.radut.net/salamanderskins/

