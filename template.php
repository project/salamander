<?php

// add jquery.pngfix.js file
drupal_add_js(drupal_get_path('theme', 'salamander') . '/js/jquery.pngfix.js', 'theme');


// Salamander's regions
function salamander_regions() {
  return array(
			'sidebar_left' => t('Left sidebar'),
			'sidebar_right' => t('Right sidebar'),
			'header' => t('Header'),
		'user1' => t('User 1'),
		'user2' => t('User 2'),
		'user3' => t('User 3'),
			'content_top' => t('Top content'),
			'content_bottom' => t('Bottom content'),
		'user4' => t('User 4'),
		'user5' => t('User 5'),
		'user6' => t('User 6'),
			'footer' => t('Footer')
       
  );
} 

/*  Modify theme variables  */
function xmll() {
	return '<em>by <a href="http://www.radut.net/"> Dr. Radut</a></em>';
}

//  Breadcrumb override
function salamander_breadcrumb($breadcrumb) {
  if (!empty($breadcrumb)) {
	  $breadcrumb[] = drupal_get_title();  // full breadcrumb
    return '<div class="breadcrumb">'. implode(' &raquo; ', $breadcrumb) .'</div>';
  }
}

// Quick fix for the validation error: 'ID "edit-submit" already defined'
$elementCountForHack = 0;
function phptemplate_submit($element) {
  global $elementCountForHack;
  return str_replace('edit-submit', 'edit-submit-' . ++$elementCountForHack, theme('button', $element));
}

